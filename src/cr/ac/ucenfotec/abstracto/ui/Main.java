package cr.ac.ucenfotec.abstracto.ui;

import cr.ac.ucenfotec.abstracto.bl.*;
import cr.ac.ucenfotec.abstracto.dl.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.ArrayList;

public class Main {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ArrayList<Cliente> clientes = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        int opcion;

        do {
            opcion = pintarMenu();
            switch (opcion){
                case 0:
                    out.println("Gracias por utilizar el programa");
                    break;
                case 1:
                    registrarCliente();
                    break;
                case 2:
                    verClientes();
                    break;
                case 3:
                    ahorroAparte();
                    break;
                case 4:
                    corrienteAparte();
                    break;
                case 5:
                    ahorroProgramadoAparte();
                    break;
                case 6:
                    movimientos();
                    break;
                case 7:
                    verSaldo();
                    break;
                default:
                    out.println("Opción invalida");
                    break;
            }
        } while (opcion != 0);
    }//fin main

    public static int pintarMenu () throws IOException{
        out.println("---MENU---");
        out.print("1. Registrar Cliente \n" +
                "2. Ver Cliente \n" +
                "3. Crear cuenta de ahorro \n" +
                "4. Crear cuenta corriente \n" +
                "5. Crear cuenta de ahorro programado \n" +
                "6. Realizar movimiento \n" +
                "6. Ver saldo \n" +
                "0. Salir \n>");
        return Integer.parseInt(in.readLine());
    }//fin pintarMenu

    public static void registrarCliente () throws IOException {
        out.println("DIGITE LA INFORMACIÓN DEL CLIENTE");
        out.print("Nombre\n>");
        String nombre = in.readLine();
        out.print("Cédula\n>");
        String id = in.readLine();
        out.print("Dirección\n>");
        String direccion = in.readLine();
        Cliente cliente = new Cliente(nombre, id, direccion);
        clientes.add(cliente);
//        ClientesBD.agregarCliente(cliente);

        char opcion;
        out.print("¿Desea agregar una cuenta? (S/N) \n>");
        opcion = in.readLine().charAt(0);
        if (Character.toLowerCase(opcion) == 's'){
            do {
                out.println("¿Que tipo de cuneta desea crear?");
                out.print("1. Cuenta de ahorro \n" +
                        "2. Cuenta Corriente \n" +
                        "3. Cuenta de Ahorro Programado \n" +
                        "0. Salir \n>");
                int tipoCuenta = Integer.parseInt(in.readLine());
                switch (tipoCuenta){
                    case 0:
                        break;
                    case 1:
                        registroAhorro(cliente);
                        break;
                    case 2:
                        registroCorriente(cliente);
                        break;
                    case 3:
                        registroAhorroProgramado(cliente);
                        break;
                    default:
                        out.println("Opción invalida\n");
                        break;
                }
                out.print("¿Desea agregar otra cuenta? (S/N) \n>");
                opcion = in.readLine().charAt(0);
            } while (Character.toLowerCase(opcion) == 's');
        }

    }//fin registrarCliente

    public static void registroAhorro(Cliente cliente) throws IOException {
        out.println("DIGITE LA INFORMACIÓN DE LA CUENTA DE AHORRO");
        out.print("Numero cuenta\n>");
        int numeroCuenta = Integer.parseInt(in.readLine());
        out.println("Fecha de creación: " + LocalDate.now());
        LocalDate fecha = LocalDate.now();
        Ahorro ahorro = new Ahorro(numeroCuenta, 0, fecha, 0.7);
        cliente.agregarCuenta(ahorro);
    }//fin registroAhorro

    public static void registroAhorroProgramado(Cliente cliente) throws IOException {
        int id;
        boolean condicion = false;

        for (int i = 0; i < cliente.getCuentas().size(); i++) {
            if (cliente.getCuentas().get(i) instanceof Corriente){
                condicion = true;
                break;
            }
        }

        if (condicion){
            out.println("¿A que cuenta corriente desea asosiar esta nueva cuenta de ahorro programado?");
            for (int i = 0; i < cliente.getCuentas().size(); i++) {
                if (cliente.getCuentas().get(i) instanceof Corriente){
                    out.println(i+". "+"Nombre: "+cliente.getNombre()+" Numero cuenta: "+cliente.getCuentas().get(i).getNumeroCuenta());
                }
            }
            out.println("Digite el número de la cuenta corriente que desea asosiar a esta nueva cuenta de ahorro programado");
            id = Integer.parseInt(in.readLine());
            Corriente corriente = (Corriente) cliente.getCuentas().get(id);

            out.println("DIGITE LA INFORMACIÓN DE LA CUENTA DE AHORRO PROGRAMADO");
            out.print("Numero cuenta\n>");
            int numeroCuenta = Integer.parseInt(in.readLine());
            out.println("Fecha de creación: " + LocalDate.now());
            LocalDate fecha = LocalDate.now();
            Ahorroprogramado ahorroProgramado = new Ahorroprogramado(numeroCuenta, 0, fecha, 0.7, corriente);
            cliente.agregarCuenta(ahorroProgramado);
        } else {
            out.println("Necesita tener al menos una cuenta corriente registrada para poder crear una de ahorro programado");
        }
    }//fin registroAhorroProgramado

    public static void registroCorriente(Cliente cliente) throws IOException {
        double saldo;
        boolean validar = false;
        out.println("DIGITE LA INFORMACIÓN DE LA CUENTA CORRIENTE");
        out.print("Numero cuenta\n>");
        int numeroCuenta = Integer.parseInt(in.readLine());
        out.println("Fecha de creación: " + LocalDate.now());
        LocalDate fecha = LocalDate.now();
        do {
            out.println("El deposito mínimo tiene que ser de 50.000");
            out.print("Depósito mínimo\n>");
            saldo = Double.parseDouble(in.readLine());
            if (saldo >= 50000){
                Corriente corriente = new Corriente(numeroCuenta, saldo, fecha);
                cliente.agregarCuenta(corriente);
                validar = true;
            }
        } while (!validar);
    }//fin registroCorriente

    public static void verClientes () throws IOException{
        for (int i = 0; i < clientes.size(); i++) {
            Cliente cliente = clientes.get(i);
            out.println(i + ". Nombre: " + cliente.getNombre() + "-Cédula: " + cliente.getId() + "-Dirección: " + cliente.getDireccion());
        }

//        out.println("Usando la DL");
//        for (Cliente cliente : ClientesBD.leerCliente()) {
//            out.println(cliente);
//        }
    }//fin verClientes

    public static void ahorroAparte () throws IOException {
        Cliente cliente = elegirCliente();
        registroAhorro(cliente);

    }//fin ahorroAparte

    public static void corrienteAparte () throws IOException {
        Cliente cliente = elegirCliente();
        registroCorriente(cliente);
    }//fin corrienteAparte

    public static void ahorroProgramadoAparte () throws IOException {
        Cliente cliente = elegirCliente();
        registroAhorroProgramado(cliente);
    }

    public static Cliente elegirCliente () throws IOException {
        verClientes();
        out.println();
        out.print("Digite el número del cliente a el que le desea agrear la cuenta de ahorro \n>");
        int id = Integer.parseInt(in.readLine());
        return clientes.get(id);
    }//fin elegirCliente

    public static void movimientos () throws IOException {
       Cliente cliente = elegirCliente();
       out.println();
        for (int i = 0; i < cliente.getCuentas().size(); i++) {
            if (cliente.getCuentas().get(i) instanceof Ahorro || cliente.getCuentas().get(i) instanceof Corriente){
                out.println(i + ". Nombre: " + cliente.getNombre() + "-Numero cuenta: " +  cliente.getCuentas().get(i).getNumeroCuenta());
            }
        }
        out.print("Digite el numero de la cuenta a el que le desea hacer el movimiento \n" +
                " (El número a el inicio de la pantalla) \n>");
        int id = Integer.parseInt(in.readLine());
        out.println("Digite el tipo de movimiento que desea realizar");
        out.print("1. Depósito\n" +
                "2. Retiro\n>");
        int condicion = Integer.parseInt(in.readLine());

        if (condicion == 1){
            out.println("Fecha del movimiento: " + LocalDate.now());
            LocalDate fecha = LocalDate.now();
            out.print("Digite el monto que desea depositar: \n>");
            double monto = Double.parseDouble(in.readLine());
            out.print("Descripción: \n>");
            String desc = in.readLine();
            Movimiento movimiento = new Movimiento(fecha, desc, monto);
            cliente.getCuentas().get(id).agregarMovimiento(movimiento);
            cliente.getCuentas().get(id).realizarDeposito(movimiento);
        } else {
            out.println("Fecha del movimiento: " + LocalDate.now());
            LocalDate fecha = LocalDate.now();
            out.print("Digite el monto que desea retirar: \n>");
            double monto = Double.parseDouble(in.readLine());
            out.print("Descripción: \n>");
            String desc = in.readLine();
            Movimiento movimiento = new Movimiento(fecha, desc, monto);
            cliente.getCuentas().get(id).agregarMovimiento(movimiento);
            cliente.getCuentas().get(id).realizarRetiro(movimiento);
        }
    }//fin movimientos

    public static void verSaldo () throws IOException {
        Cliente cliente = elegirCliente();

        for (int i = 0; i < cliente.getCuentas().size(); i++) {
            out.println("El saldo disponible de la cuenta: "+cliente.getCuentas().get(i).getNumeroCuenta()+" es de: "+
                    cliente.getCuentas().get(i).getSaldo());
        }
    }//fin verSaldo

}//fin programa