package cr.ac.ucenfotec.abstracto.dl;

import cr.ac.ucenfotec.abstracto.bl.Cliente;

import java.io.*;
import java.util.ArrayList;

public class ClientesBD {

    private static final String NOMBRE = "clientes.csv";
    private static final String SEPARADOR = ",";

    public static void agregarCliente(Cliente cliente) throws IOException {
        FileWriter escritorDeArchivo = new FileWriter(NOMBRE, true);
        Writer escritor = new BufferedWriter(escritorDeArchivo);
        escritor.append(cliente.getNombre() + SEPARADOR + cliente.getId() + SEPARADOR + cliente.getDireccion() + "\n");
        escritor.close();
    }

    public static ArrayList<Cliente> leerCliente() throws IOException {
        // Leer el archivo de texto (.csv)
        // Con cada linea va a crear un osito nuevo
        // Va a colocar los ositos en una arreglo
        // Me los va a devolver

        ArrayList<Cliente> clientes = new ArrayList<>();

        File archivo = new File(NOMBRE);
        FileReader lector = new FileReader(archivo);
        BufferedReader buffer = new BufferedReader(lector);

        String linea;
        while ((linea = buffer.readLine()) != null) {
            String[] split = linea.split(SEPARADOR);

            String nombre = split[0];
            String id = (split[1]);
            String direccion = (split[2]);

            Cliente cliente = new Cliente(nombre, id, direccion);
            clientes.add(cliente);
        }
        return clientes;
    }

}
