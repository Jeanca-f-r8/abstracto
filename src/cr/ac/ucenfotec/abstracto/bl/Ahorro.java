package cr.ac.ucenfotec.abstracto.bl;

import java.time.LocalDate;

public class Ahorro extends Cuenta{

    private double intereses;

    public Ahorro(){
        setIntereses(0.0);
    }

    public Ahorro(int numeroCuenta, double saldo, LocalDate fechaInicio, double intereses) {
        super(numeroCuenta, saldo, fechaInicio);
        setIntereses(intereses);
    }

    public double getIntereses() {
        return intereses;
    }

    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }

    @Override
    public void realizarDeposito(Movimiento movimiento) {
        setSaldo(getSaldo() + movimiento.getMonto());
    }

    @Override
    public void realizarRetiro(Movimiento movimiento) {
        if (getSaldo() > 100000){
            if (movimiento.getMonto() > (getSaldo()/2)){
                setSaldo(getSaldo() - movimiento.getMonto());
            }
        }
    }

    @Override
    public String toString() {
        return "Ahorro{" +
                "intereses=" + intereses +
                '}';
    }
}
