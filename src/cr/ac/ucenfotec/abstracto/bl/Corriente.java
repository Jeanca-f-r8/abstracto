package cr.ac.ucenfotec.abstracto.bl;

import java.time.LocalDate;

public class Corriente extends Cuenta {


    public Corriente(){
    }

    public Corriente(int numeroCuenta, double saldo, LocalDate fechaInicio) {
        super(numeroCuenta, saldo, fechaInicio);
    }

    @Override
    public void realizarDeposito(Movimiento movimiento) {
        setSaldo(getSaldo() + movimiento.getMonto());
    }

    @Override
    public void realizarRetiro(Movimiento movimiento) {
        setSaldo(getSaldo() - movimiento.getMonto());
    }


    @Override
    public String toString() {
        return "Corriente{" +
                '}';
    }
}
