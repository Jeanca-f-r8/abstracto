package cr.ac.ucenfotec.abstracto.bl;

import cr.ac.ucenfotec.abstracto.ui.Main;

import java.time.LocalDate;
import java.util.ArrayList;

public abstract class Cuenta {

    private int numeroCuenta;
    private double saldo;
    private LocalDate fechaInicio;
    private ArrayList<Movimiento> movimientos = new ArrayList<>();

    public Cuenta () {
        setNumeroCuenta(0);
        setSaldo(0.0);
        setFechaInicio(LocalDate.parse("01/01/2021"));
    }

    public Cuenta(int numeroCuenta, double saldo, LocalDate fechaInicio) {
        setNumeroCuenta(numeroCuenta);
        setSaldo(saldo);
        setFechaInicio(fechaInicio);
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        if (this.numeroCuenta < 9999999 && this.numeroCuenta > 1000000){
            this.numeroCuenta = numeroCuenta;
        } else {
            this.numeroCuenta = (int) (Math.random()*9999999+1000000);
        }
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ArrayList<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(ArrayList<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }

    public void agregarMovimiento (Movimiento movimiento) {
        movimientos.add(movimiento);
    }

    abstract public void realizarDeposito(Movimiento movimiento);

    abstract public void realizarRetiro(Movimiento movimiento);

    @Override
    public String toString() {
        return "Cuenta{" +
                "numeroCuenta='" + numeroCuenta + '\'' +
                ", saldo=" + saldo +
                ", fechaInicio=" + fechaInicio +
                ", movimientos=" + movimientos +
                '}';
    }
}
