package cr.ac.ucenfotec.abstracto.bl;

import java.time.LocalDate;

public class Ahorroprogramado extends Ahorro{

    private Corriente corriente;

    public Ahorroprogramado() {
        setCorriente(null);
    }

    public Ahorroprogramado(int numeroCuenta, double saldo, LocalDate fechaInicio, double intereses, Corriente corriente) {
        super(numeroCuenta, saldo, fechaInicio, intereses);
        setCorriente(corriente);
    }

    public Corriente getCorriente() {
        return corriente;
    }

    public void setCorriente(Corriente corriente) {
        this.corriente = corriente;
    }

    @Override
    public void realizarDeposito(Movimiento movimiento) {
       //deposito automatico por mes
    }

    @Override
    public void realizarRetiro(Movimiento movimiento) {
        String fecha = String.valueOf(getFechaInicio());
        String [] fechaArray = fecha.split("-");

        LocalDate fechaHoy = LocalDate.now();
        String fechaSplit = String.valueOf(fechaHoy);
        String [] fechaHoyArray = fechaSplit.split("-");

        if (fechaArray[0].equals(fechaHoyArray[0])) {
            if (fechaArray[1].equals(fechaHoyArray[1]))
                if (fechaArray[2].equals(fechaHoyArray[2]))
            setSaldo(getSaldo() - movimiento.getMonto());
        }
    }

    @Override
    public String toString() {
        return "Ahorroprogramado{" +
                "corriente=" + corriente +
                '}';
    }
}
