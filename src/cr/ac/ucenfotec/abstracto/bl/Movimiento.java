package cr.ac.ucenfotec.abstracto.bl;

import java.time.LocalDate;

public class Movimiento {

    private LocalDate fecha;
    private String descripcion;
    private double monto;

    public Movimiento(){
        setFecha(LocalDate.parse("01/01/2021"));
        setDescripcion("Sin cuenta");
        setMonto(0.0);
    }

    public Movimiento(LocalDate fecha, String descripcion, double monto) {
        setFecha(fecha);
        setDescripcion(descripcion);
        setMonto(monto);
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    @Override
    public String toString() {
        return "Movimiento{" +
                "fecha=" + fecha +
                ", descipcion='" + descripcion + '\'' +
                ", monto=" + monto +
                '}';
    }
}
