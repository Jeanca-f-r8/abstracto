package cr.ac.ucenfotec.abstracto.bl;

import java.util.ArrayList;

public class Cliente {

    private String nombre;
    private String id;
    private String direccion;
    private ArrayList<Cuenta> cuentas = new ArrayList<>();

    public Cliente (){
        setNombre("Sin nombre");
        setId("Sin ID");
        setDireccion("Sin dirección");
    }

    public Cliente(String nombre, String id, String direccion) {
        setNombre(nombre);
        setId(id);
        setDireccion(direccion);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ArrayList<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(ArrayList<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public void agregarCuenta (Cuenta cuenta) {
        cuentas.add(cuenta);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", id='" + id + '\'' +
                ", direccion='" + direccion + '\'' +
                ", cuentas=" + cuentas +
                '}';
    }
}
